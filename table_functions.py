import csv
import os
from pprint import pprint

from log_functions import write_log


def create_file(name, path="tables/"):
    return open(path + name + ".csv", 'x')


def delete_file(name, path="tables/"):
    os.remove(path + name + ".csv")


def create_table(table_name, headers):
    with create_file(table_name) as f:
        writer = csv.DictWriter(f, fieldnames=headers)
        writer.writeheader()

    write_log(f"'{table_name}' {headers} created.")


def delete_table(table_name):
    try:
        delete_file(table_name)
        write_log(f"'{table_name}' deleted")
    except:
        pass


def open_table(name, path="tables/", mode='r'):
    return open(path + name + ".csv", mode)


def insert(table_name, values):
    with open_table(table_name, mode="a") as f:
        writer = csv.writer(f)
        writer.writerow(values)
    write_log(f"{values} added to '{table_name}'")


def select(table_name, target=None, conditions=None):
    if conditions is None:
        conditions = {}
    if target is None:
        target = "all"
    result = []
    with open_table(table_name, mode='r') as f:
        reader = csv.DictReader(f)
        if target == "all":
            headers = reader.fieldnames
        else:
            headers = target

        for line in reader:
            row = []
            for header in headers:
                row.append(line[header])

            conditions_count = -1

            if len(conditions) > 0:
                conditions_count = 0

                for key in conditions:
                    if str(conditions[key]) == str(line[key]):
                        conditions_count += 1

            if conditions_count == len(conditions) or conditions_count == -1:
                result.append(row)

    return result


if __name__ == '__main__':
    pprint(select("example", conditions={"profession": "teacher"}))
