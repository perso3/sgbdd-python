from datetime import datetime


def create_log_file(name, path="logs/"):
    return open(path + name + ".log")


def open_log_file(name, path="logs/", mode='a'):
    return open(path + name + ".log", mode)


def write_log(info, file="general"):
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    with open_log_file(file) as f:
        f.write(dt_string + " >>> " + info + "\n")
